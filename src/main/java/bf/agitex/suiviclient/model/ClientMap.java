package bf.agitex.suiviclient.model;

import com.univocity.parsers.annotations.Parsed;
import com.univocity.parsers.annotations.Trim;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Classe utilisé pour le mapping des données d'un fichier.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientMap implements Serializable {
    @Trim
    @Parsed
    private String nom;
    @Trim
    @Parsed
    private String prenom;
    @Trim
    @Parsed
    private Integer age;
    @Trim
    @Parsed
    private Integer revenu;
    @Trim
    @Parsed
    private String profession;
}
