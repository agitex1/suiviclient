package bf.agitex.suiviclient.service.util;

import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

public class Util {
    public static final String CHARACTER_ENCODING = "UTF-8";
    public static final String ROOT_PATH = "src/main/resources/assets/";

    public static CsvParser setParserSettings(BeanListProcessor<?> rowProcessor) {
        CsvParserSettings parserSettings = new CsvParserSettings();
        parserSettings.setRowProcessor(rowProcessor);
        parserSettings.setHeaderExtractionEnabled(true);
        parserSettings.setDelimiterDetectionEnabled(true);
        parserSettings.setAutoConfigurationEnabled(true);
        return new CsvParser(parserSettings);
    }

}
