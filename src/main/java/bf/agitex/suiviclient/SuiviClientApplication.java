package bf.agitex.suiviclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuiviClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuiviClientApplication.class, args);
	}

}
