package bf.agitex.suiviclient.service;

import bf.agitex.suiviclient.model.ClientMap;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static bf.agitex.suiviclient.service.util.Util.*;

@Service
public class ImportService {
    /**
     * Chargements des clients à partir d'un fichier csv.
     * @return List Client
     */
    public Set<ClientMap> getClientsFromCsv(String nomFichier) {
        String filePath = ROOT_PATH.concat(nomFichier);
        BeanListProcessor<ClientMap> rowProcessor = new BeanListProcessor<>(ClientMap.class);
        CsvParser parser = setParserSettings(rowProcessor);
        parser.parse(new File(filePath), CHARACTER_ENCODING);
        return new HashSet<>(rowProcessor.getBeans());
    }
    /**
     * Chargements des clients à partir d'un fichier json.
     * @return List Client
     */
    public Set<ClientMap> getClientsFromJson(String nomFichier) {
        ObjectMapper mapper = new ObjectMapper();
        String filePath = ROOT_PATH.concat(nomFichier);
        try {
            Set<ClientMap> clientCsvs = mapper.readValue(new File(filePath), new TypeReference<Set<ClientMap>>() {});
            return clientCsvs;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Chargements des clients à partir d'un fichier xml.
     * @return List Client
     */
    public Set<ClientMap> getClientsFromXml(String nomFichier) {
        String filePath = ROOT_PATH.concat(nomFichier);
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("Client");
            Set<ClientMap> userList = new HashSet<>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                userList.add(getClientNode(nodeList.item(i)));
            }
            return userList;
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Extraction des informations du client.
     * @param node
     * @return
     */
    private static ClientMap getClientNode(Node node) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            ClientMap client = new ClientMap();
            client.setNom(getTagValue("nom", element));
            client.setPrenom(getTagValue("prenom", element));
            client.setAge(Integer.parseInt(getTagValue("age", element)));
            client.setProfession(getTagValue("profession", element));
            client.setRevenu(Integer.parseInt(getTagValue("revenu", element)));
            return client;
        } else return null;
    }

    /**
     * recuperation de la valeur du noead.
     * @param tag
     * @param element
     * @return valeur
     */
    private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag)
                .item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }
}
