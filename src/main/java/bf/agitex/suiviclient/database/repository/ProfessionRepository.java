package bf.agitex.suiviclient.database.repository;

import bf.agitex.suiviclient.database.entity.Profession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfessionRepository extends JpaRepository<Profession, Long> {
    /**
     * Recherche de profession en fonction du nom.
     * @param nom
     * @return profession
     */
    Optional<Profession> findByNom(String nom);
}
