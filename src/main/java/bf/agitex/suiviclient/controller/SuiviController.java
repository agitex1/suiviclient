package bf.agitex.suiviclient.controller;

import bf.agitex.suiviclient.service.SuiviService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequiredArgsConstructor
@RestController("suivi")
public class SuiviController {
    private final SuiviService suiviService;

    /**
     * Chargement de client à partir d'un fichier.
     * @param format
     * @param nomFichier
     * @return
     */
    @PostMapping("/chargement-clients")
    public ResponseEntity<Boolean> loadClients(@RequestParam String nomFichier, @RequestParam String format) {
        suiviService.loadClients(nomFichier,  format);
        return ResponseEntity.ok(Boolean.TRUE);
    }


    /**
     *Calcul de moyenne des salaires en fonction de la profession.
     * @return moyenne
     */
    @GetMapping("/moyenne-salaire")
    public ResponseEntity<Double> getMoyenneRevenuByProfession(@RequestParam Long professionId) {
        return ResponseEntity.ok(suiviService.getMoyenneRevenuByProfession(professionId));
    }
}
