package bf.agitex.suiviclient.service;

import bf.agitex.suiviclient.database.entity.Client;
import bf.agitex.suiviclient.database.entity.Profession;
import bf.agitex.suiviclient.database.repository.ClientRepository;
import bf.agitex.suiviclient.database.repository.ProfessionRepository;
import bf.agitex.suiviclient.model.ClientMap;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public   class SuiviService {
    private final ClientRepository clientRepository;
    private final ProfessionRepository professionRepository;
    private final ImportService importService;
    /**
     * Chargement des données de client à travers un fichier csv, xml, json.
     * @param nomFichier
     */
    public void loadClients(String nomFichier, String format) {
        Set<ClientMap> clients = new HashSet<>();
        switch (format) {
            case "csv":
                clients = importService.getClientsFromCsv(nomFichier.concat(".").concat(format));
                break;
            case "xml":
                clients = importService.getClientsFromXml(nomFichier.concat(".").concat(format));
                break;
            case "json":
                clients = importService.getClientsFromJson(nomFichier.concat(".").concat(format));
                break;
        }
        clients.stream().forEach(c -> {
            Optional<Profession> professionExist = getProfessionByNom(c.getProfession());
            Client client = new Client();
            client.setNom(c.getNom());
            client.setPrenom(c.getPrenom());
            client.setAge(c.getAge());
            client.setRevenu(c.getRevenu());
            client.setProfession(professionExist.isPresent()? professionExist.get()
                            : saveProfession(new Profession( null, c.getProfession())));
            saveClient(client);
        });
    }

    /**
     * Service permettant d'enregistrer une profession.
     */
    public Profession saveProfession(Profession profession) {
        return professionRepository.saveAndFlush(profession);
    }


    /**
     * Service permettant d'enregistrer un client.
     * TODO verfie l'existence d'une client à partir d'une clé unicité avant l'enregistrement
     */
    public Client saveClient(Client client) {
        return clientRepository.save(client);
    }
    /**
     * Rechereche d'une prfession en fonction du nom.
     * @param nom
     * @return profession
     */
    public Optional<Profession> getProfessionByNom(String nom) {
        return professionRepository.findByNom(nom);
    }

    /**
     * Calcul de la moyenne des salaires par type de profession.
     * @return moyenne
     */
    public Double getMoyenneRevenuByProfession(Long professionId) {
        List<Client> clients = clientRepository.findByProfessionId(professionId);
        Double moyenne = clients.stream().mapToInt(s -> s.getRevenu()).average().getAsDouble();
        return moyenne;
    }
}
