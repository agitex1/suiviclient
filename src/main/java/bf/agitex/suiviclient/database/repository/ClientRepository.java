package bf.agitex.suiviclient.database.repository;

import bf.agitex.suiviclient.database.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    /**
     * Recherche de client en fonction de la profession.
     * @param professionId
     * @return List Client
     */
    List<Client> findByProfessionId(Long professionId);
}
