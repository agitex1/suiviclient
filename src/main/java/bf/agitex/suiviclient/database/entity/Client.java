package bf.agitex.suiviclient.database.entity;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

/**
 * Les informations de base d'un client.
 */
@Entity
@Data
public class Client {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    @Column(nullable = false)
    private String nom;
    @Column(nullable = false)
    private String prenom;
    @Column(nullable = false)
    private Integer age;
    @Column(nullable = false)
    private Integer revenu;
    @ManyToOne
    private Profession profession;

    public Client() {

    }
}
